#!/bin/sh
#warn user when the distro reach their end of life (eol)
set -x

autostartFile="$HOME/.config/autostart/eol-notify.desktop"
version=$(grep -o "^[0-9]*" /etc/debian_version)

case $version in
#https://wiki.debian.org/LTS
11) eof=$(date +%s -d "31 August 2026"); codename="Bullseye";;
12) eof=$(date +%s -d "30 June 2028"); codename="Bookworm";;
esac
now=$(date +%s)
delayed=$(date -r "$autostartFile" '+%s') || delayed=0
daysLeft=$((($eof-$now)/60/60/24))

if test $delayed -lt $now
then
	if [ $daysLeft -lt 14 ]
	then
		m1="Debian $version ($codename) expire dans $daysLeft jours"
		m2="Votre système bénéficie de mises à jour de sécurité.
		Une mise à niveau du système sera bientôt nécessaire !
		C'est une fois tous les cinq ans chez Debian."
		m3="dialog-warning"
	elif [ $daysLeft -lt 0 ]
	then
		m1="Debian $version ($codename) est expiré depuis $daysLeft jours"
		m2="Vous ne bénéficiez plus de mises à jour de sécurité !
		Si vous ne savez pas comment mettre à niveau le système,
		contactez le Groupe d'Utilisateur Linux (GUL) le plus proche !"
		m3="dialog-error"
	fi	
fi

if test -n "$m1"
then
	return=$(notify-send -i $m3 "$m1" "$m2" -A "Ignorer" -A "delay=Rappel dans 7 jours" -A "disable=Ne plus afficher" -t 60000)
	if test "$return" = 'delay'
	then
	  touch -d "$(date +%F -d '7 days')" "~/.config/autostart/eol_notify.desktop"
	elif test "$return" = 'disable'
	then
	  rm "$autostartFile"
	fi
fi